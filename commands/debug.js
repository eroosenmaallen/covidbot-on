/**
 *  @author     Eddie Roosenmaallen <silvermoon82@gmail.com>
 *  @date       June 2022
 *
 *  This command fetches a CSV 
 *
 *  This is about the simplest possible implementation of a covidbot pipeline.
 */

const { fetchCsv } = require('../lib/util.js')

Object.assign(exports, {
  command: 'debugCsv <url>',
  description: 'Fetch a CSV and print what we end up knowing about it',
  builder: {
    url: {
      desc: 'URL of the csv to fetch',
      type: 'string',
    },
    phuNum: {
      desc: 'Public Health Unit id number; will be checked against the PHU_NUM column',
      type: 'number',
      cast: true,
    },
    ohRegion: {
      desc: 'Public Health Region name; will filter against the oh_region column',
      type: 'string',
    }
  },
  handler,
});

async function handler(cli) {
  const url = cli.url;

  return fetchCsv(url, row => {
    if (cli.phuNum && row.PHU_NUM && row.PHU_NUM !== cli.phuNum)
      return false;
    if (cli.ohRegion && row.oh_region && row.oh_region !== cli.ohRegion)
      return false;

    return true;
  })
  .then(rows => {
    console.log('Last row:');
    console.log(rows.slice(-1));
    return rows;
  });
}
