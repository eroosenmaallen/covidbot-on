#! /usr/bin/env node
/**
 *  @author     Eddie Roosenmaallen <silvermoon82@gmail.com>
 *  @date       June 2022
 *
 *  This is the cli frontend for Covidbot-ON
 */

'use strict';

require('dotenv').config();

const yargs = require('yargs');

// Use the yargs command framework to actually do stuff...
yargs(require('process').argv.slice(2))
  .options({
      'post': {
        desc: 'Post the toot to Mastodon',
        type: 'boolean',
        default: false,
        global: true,
      },
      'force': {
        desc: 'Do stuff, even if we normally wouldn\'t (eg. post again on same day)',
        type: 'boolean',
        default: false,
        global: true,
      },
      visibility: {
        desc: 'Post visibility',
        default: 'public',
        global: true,
      },
      verbose: {
        desc: 'Print extra debug output',
        alias: 'v',
        global: true,
        type: 'count',
      },
    })
  .commandDir('./commands/')
  .strict()
  .demandCommand()
  .parse();
