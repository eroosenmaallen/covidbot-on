## Covidbot-ON

This bot posts available COVID numbers for public health regions and units in Ontaro.

Most data is sourced from [Ontario Public Health](https://covid-19.ontario.ca/data).

Data from KHSC is included in the mainline repo, both to illustrate handling data from
other sources and because I'm not about to fork my own repo to add my own local data.

Source is available from [GitLab](https://gitlab.com/eroosenmaallen/covidbot-on); feedback
is always welcome, or fork it and build your own!

Libraries are documented at [GitLab Pages](https://eroosenmaallen.gitlab.io/covidbot-on)


### Data Sources

* Ontario Public Health:
  * [Cases by PHU](https://data.ontario.ca/en/datastore/dump/d1bfe1ad-6575-4352-8302-09ca81f7ddfc?bom=True)
  * [Hospitalizations by Region](https://data.ontario.ca/dataset/8f3a449b-bde5-4631-ada6-8bd94dbc7d15/resource/e760480e-1f95-4634-a923-98161cfb02fa/download/region_hospital_icu_covid_data.csv)
* [Kingston Health Science Center](https://kingstonhsc.ca/patients-families-and-visitors/covid-19-information/current-covid-19-statistics)


### Technical Details

Web scraping is accomplished via [node-fetch](https://npmjs.com/package/node-fetch) and [cheerio](https://cheerio.js.org)

Toots are posted using [MastoBot](https://gitlab.com/eroosenmaallen/mastobot)
