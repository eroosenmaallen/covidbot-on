/**
 *  @author     Eddie Roosenmaallen <silvermoon82@gmail.com>
 *  @date       June 2022
 *
 *  This module handles fetching & posting data from the Kingston Health
 *    Science Center, conveniently demonstrating how to build a more complex
 *    fetcher within the covidbot framework.
 */

// Common imports
const {
  cheerio,
  fetch,
  getDeltas,
  loadLatest,
  logToot,
  postToot,
  saveLatest,
} = require('../lib/util.js');

Object.assign(exports, {
  command: 'khsc',
  describe: 'Hospitalization data from KHSC',
  handler,
});

async function handler(cli) {
  //const url = 'https://kingstonhsc.ca/patients-families-and-visitors/covid-19-information/current-covid-19-statistics';
  const url = 'https://kingstonhsc.ca/visiting-or-attending-khsc/covid-19-information/current-covid-19-statistics-outbreak-information';

  const response = await fetch(url);

  if (response.status < 200 || response.status > 299) {
    console.error('failed to fetch;', response.status, response.statusText);
    return false;
  }

  const page = await response.text();
  const $ = await cheerio.load(page);

  const text = $('div.node__content').text();

  if (cli.verbose >= 1)
    console.debug('034: content text:', text);

  const main_stats = text.match(/(Total COVID-positive)(.+?):\s*(\d+)/gm);
  const icu_stats = text.match(/(Patients in Intensive)(.+?):\s*(\d+)/gm);
  const acute_stats = text.match(/(Patients in acute)(.+?):\s*(\d+)/gm);

  main_stats.push(...icu_stats, ...acute_stats);

  // if (cli.verbose >= 2)
  //   console.log(text);

  if (cli.verbose >= 1) {
    console.log('043: main_stats:', main_stats);
  }


  // const fields = $('div.field strong');
  // const ob = text.match(/(Current Outbreaks):\s*(\d+|There are[^\.]+\.)?/gm);
  // const fieldText = ob ? ob[0] : '';


  // if (cli.verbose >= 1)
  //   console.debug('050: fields text:', fieldText);

  const outbreaks = text.match(/outbreak.+?on \S+ \d+/gi);

  if (cli.verbose >= 1)
    console.debug(`056: outbreaks:`, outbreaks);


  const today = new Date().toDateString();

  const latest = loadLatest('khsc');

  if (latest.date === today && !cli.force) {
    console.error('Already logged data for ' + latest.date);
    return false;
  }

  // assess and assemble external transfers if relevant
//  const xfers = parseInt(main_stats?.[4]?.split(/\s+/).slice(-1)[0]);
 //  const xfer_note = xfers ? `\n
 // * ${transfers.slice(0, 3).join('\n * ')}` : false;

  const spoiler_text = `Daily KHSC COVID hospitalizations, ${today}`;
  let toot = `Kingston Health Science Center COVID hospitalizations, ${today}:\n`;



  if (main_stats)
  {
    const statFields = {
      total: 'Total COVID-positive patients',
      icu: 'Patients in Intensive Care Unit (ICU)',
      acute: 'Patients in acute care',
    };
    const today = linesToObj(main_stats);
    const yesterday = linesToObj(latest?.data?.main_stats);
    const deltas = getDeltas(today, yesterday, statFields,
      diff => (diff === 0 ? '(no change)'
          : diff > 0 ? `(+${diff})`
          : `(${diff})`)
      );

    if (cli.verbose >= 1)
      console.debug(`100: main_stats:`, {statFields, today, yesterday, deltas});

    toot += Object.entries(statFields).map(([field, label]) => {
      return `\n * ${label}: ${today[label]} ${deltas[field] || ''}`;
    }).join('')
  }
  else
  {
    toot += '\n(detailed KHSC hospitalization data unavailable)';
  }

  // if (xfer_note)
  //   toot += '\n\n' + xfer_note;

  if (outbreaks?.length)
  {
//    const delta = getDeltas(
//      { length: outbreaks.length },
//      { length: latest?.data?.outbreaks?.length },
//      { length: 'length' }
//      );
    toot += `\n\nDeclared Outbreaks: ${outbreaks.length || '(no change)'}` + outbreaks.map(ob => `\n * ${ob.replace(/ - .*$/, '')}`).join('');
  }
  else
  {
    toot += `\n\nDeclared Outbreaks: none`;
  }

  toot += `\n\n#covid19 #KingstonOntario #ygk #khsc`;

  logToot(toot, spoiler_text);

  if (!cli.post)
    return;

  return postToot(toot, cli.visibility, spoiler_text)
    .then(tootPostedResult => {
      // save numbers after toot is posted successfully
      saveLatest('khsc', today, {
        main_stats,
        // transfers,
        outbreaks,
      });

      return tootPostedResult;
    });
}

/** Convert a set of lines in the format "some text:  123" into an
 *  object { "some text": 123 }
 */
function linesToObj(lines)
{
  return lines.reduce((ax, l) => {
    const [k, n] = l.split(/:\s+/);
    ax[k] = parseInt(n, 10);
    return ax;
  }, {})
}
