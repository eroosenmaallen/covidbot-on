/**
 *  @author     Eddie Roosenmaallen <silvermoon82@gmail.com>
 *  @date       June 2022
 *
 *  This command fetches case count data from Ontario Public Health and
 *    Toots it.
 *
 *  This is about the simplest possible implementation of a covidbot
 *    pipeline.
 */

const { do_csv_post, getDeltas, loadLatest, saveLatest } = require('../lib/util.js')

Object.assign(exports, {
  command: 'cases [phuId]',
  description: 'Case counts for an Ontario Public Health unit',
  builder: {
    phuId: {
      desc: 'ID for the public health unit',
      default: parseInt(require('process').env.PHU_NUM, 10) || undefined,
      type: 'number',
      cast: true,
    }
  },
  handler: cli => do_csv_post(
    cli,
    'https://data.ontario.ca/en/datastore/dump/d1bfe1ad-6575-4352-8302-09ca81f7ddfc?bom=True',

    // Make the spoiler_text with a function
    today => `Weekly COVID case counts for ${today.PHU_NAME}, ${today.FILE_DATE.slice(0, 10)}`,

    // filterFn: only rows for the specified PHU
    row => (row.PHU_NUM === cli.phuId),
    
    (today, rows) => tootMaker(cli, today, rows),
  ),
});


function tootMaker(cli, today, rows) {
  const [ yesterday ] = rows.slice(-2);

  const latest = loadLatest('cases');

  if (latest.date === today.FILE_DATE.slice(0, 10) && !cli.force) {
    console.error('Already logged data for ' + latest.date);
    return false;
  }

  // only save today's data if we posted it...
  if (cli.post)
    saveLatest('cases', today.FILE_DATE.slice(0, 10), today);

  const deltas = getDeltas(today, latest.data || yesterday, {
    cases: 'ACTIVE_CASES',
    resolved: 'RESOLVED_CASES',
    deaths: 'DEATHS',
  }, diff => (diff === 0 ? '(no change)'
      : diff > 0 ? `(+${diff})`
      : `(${diff})`)
  );

  return `Weekly COVID case counts for ${today.PHU_NAME}, ${today.FILE_DATE.slice(0, 10)}

 * Current Active Cases: ${today.ACTIVE_CASES} ${deltas.cases && deltas.cases.replace(/(\)?)$/, ` since ${latest.date}$1`)}

 * Cumulative resolved cases: ${today.RESOLVED_CASES} ${deltas.resolved}
 * Cumulative deaths: ${today.DEATHS} ${deltas.deaths}

#covid #covid19 #KingstonOntario`
}